<footer class="footer">
  <div class="container-fluid clearfix">
    <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2017 <a href="#">UrbanUI</a>. All rights reserved.</span>
    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
  </div>
</footer>
<!-- partial -->
</div>
<!-- content-wrapper ends -->
</div>
<!-- row-offcanvas ends -->
</div>
<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->
<!-- plugins:js -->
<script src="..\template\vendors\js\vendor.bundle.base.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="..\template\js\off-canvas.js"></script>
<script src="..\template\js\hoverable-collapse.js"></script>
<script src="..\template\js\misc.js"></script>
<script src="..\template\js\settings.js"></script>
<script src="..\template\js\todolist.js"></script>

<script src="..\template\vendors\datatables.net\jquery.dataTables.js"></script>
<script src="..\template\vendors\datatables.net-bs4\dataTables.bootstrap4.js"></script>
<script src="..\template\vendors\select2\select2.min.js"></script>
<script src="../lib/sweetalert.min.js"></script>

<script>
  (function($) {
    'use strict';
    $(function() {
      $('#order-listing').DataTable({
        "aLengthMenu": [
          [5, 10, 15, -1],
          [5, 10, 15, "All"]
        ],
        "iDisplayLength": 10,
        "language": {
          search: ""
        }
      });
      $('#order-listing').each(function() {
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.removeClass('form-control-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.removeClass('form-control-sm');
      });
    });
  })(jQuery);

  (function($) {
    'use strict';

    if ($(".select2").length) {
      $(".select2").select2();
    }
    if ($(".select2multiple").length) {
      $(".select2multiple").select2();
    }
  })(jQuery);
</script>
<!-- endinject -->
<!-- Custom js for this page-->
<!-- End custom js for this page-->
</body>

</html>