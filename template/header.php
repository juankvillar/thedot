
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>The Dot Studio</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="..\template\vendors\mdi\css\materialdesignicons.min.css">
  <link rel="stylesheet" href="..\template\vendors\simple-line-icons\css\simple-line-icons.css">
  <link rel="stylesheet" href="..\template\vendors\flag-icon-css\css\flag-icon.min.css">
  <link rel="stylesheet" href="..\template\vendors\css\vendor.bundle.base.css">

  <!-- libs -->
  <link rel="stylesheet" href="..\template\vendors\datatables.net-bs4\dataTables.bootstrap4.css">
  <link rel="stylesheet" href="..\template\vendors\select2\select2.min.css">
  <link rel="stylesheet" href="..\template\vendors\select2-bootstrap-theme\select2-bootstrap.min.css">
  <!-- /libs -->
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="..\template\css\style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="..\template\images\favicon.png">
</head>

<body>
  <div class="container-scroller">
    <!-- partial:../../partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="..\index.html"><img src="..\template\images\logo.svg" alt="logo"></a>
        <a class="navbar-brand brand-logo-mini" href="..\index.html"><img src="..\template\images\logo-mini.svg" alt="logo"></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
          <span class="icon-menu"></span>
        </button>
        
        <ul class="navbar-nav navbar-nav-right">
          
          <li class="nav-item nav-settings d-none d-lg-block">
            <a class="nav-link" href="#">
              <i class="icon-grid"></i>
            </a>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="icon-menu"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <div class="row row-offcanvas row-offcanvas-right">
        <div id="right-sidebar" class="settings-panel">
          <i class="settings-close mdi mdi-close"></i>
          <ul class="nav nav-tabs" id="setting-panel" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="todo-tab" data-toggle="tab" href="#todo-section" role="tab" aria-controls="todo-section" aria-expanded="true">TO DO LIST</a>
            </li>
          </ul>
          <div class="tab-content" id="setting-content">
            <div class="tab-pane fade show active scroll-wrapper" id="todo-section" role="tabpanel" aria-labelledby="todo-section">
              <div class="events py-4 border-bottom px-3">
                <div class="wrapper d-flex mb-2">
                  <i class="mdi mdi-circle-outline text-primary mr-2"></i>
                  <span>Feb 11 2018</span>
                </div>
                <p class="mb-0 font-weight-thin text-gray">Creating component page</p>
                <p class="text-gray mb-0">build a js based app</p>
              </div>
              <div class="events pt-4 px-3">
                <div class="wrapper d-flex mb-2">
                  <i class="mdi mdi-circle-outline text-primary mr-2"></i>
                  <span>Feb 7 2018</span>
                </div>
                <p class="mb-0 font-weight-thin text-gray">Meeting with Alisa</p>
                <p class="text-gray mb-0 ">Call Sarah Graves</p>
              </div>
            </div>
            <!-- To do section tab ends -->
            
            <!-- chat tab ends -->
          </div>
        </div>
        <!-- partial -->
        <!-- partial:../../partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            <li class="nav-item nav-profile">
              <div class="nav-link">
                <div class="profile-image">
                  <img src="..\template\images\faces\<?php echo $_SESSION['imagen'];?>" alt="image">
                  <span class="online-status online"></span> <!--change class online to offline or busy as needed-->
                </div>
                <div class="profile-name">
                  <p class="name">
                    <?php echo $_SESSION['nombre']; ?>
                  </p>
                  <p class="designation">
                    Super Admin
                  </p>
                </div>
              </div>
            </li>
            <?php if ($_SESSION['id_rol'] == 1) { ?>
            <li class="nav-item">
              <a class="nav-link" href="capacitaciones.php">
                <i class="icon-grid menu-icon"></i>
                <span class="menu-title">Capacitaciones</span>
                <!-- <span class="badge badge-success">New</span> -->
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="programaciones.php">
                <i class="icon-cursor menu-icon"></i>
                <span class="menu-title">Programación</span>
                <!-- <span class="badge badge-success">New</span> -->
              </a>
            </li>
            <?php }?>
            <li class="nav-item">
              <a class="nav-link" href="inscripciones.php">
                <i class="icon-calendar menu-icon"></i>
                <span class="menu-title">Inscripciones</span>
                <!-- <span class="badge badge-success">New</span> -->
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="controllers\logout.php">
                <i class="icon-close menu-icon text-danger"></i>
                <span class="menu-title">Salir</span>
              </a>
            </li>
            </li>
          </ul>
        </nav>
        <!-- partial -->
        <div class="content-wrapper">