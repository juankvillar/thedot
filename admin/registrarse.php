<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Victory Admin</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="..\template\vendors\mdi\css\materialdesignicons.min.css">
  <link rel="stylesheet" href="..\template\vendors\simple-line-icons\css\simple-line-icons.css">
  <link rel="stylesheet" href="..\template\vendors\flag-icon-css\css\flag-icon.min.css">
  <link rel="stylesheet" href="..\template\vendors\css\vendor.bundle.base.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="..\template\css\style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="..\template\images\favicon.png">
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper">
      <div class="row">
        <div class="content-wrapper full-page-wrapper d-flex align-items-center auth register-full-bg">
          <div class="row w-100">
            <div class="col-lg-4 mx-auto">
              <div class="auth-form-light text-left p-5">
                <h2>Registrate</h2>
                <form class="pt-4" id="frm">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nombres</label>
                    <input type="text" class="form-control" id="nombres" name="nombres" required>
                    <i class="mdi mdi-account"></i>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Apelidos</label>
                    <input type="text" class="form-control" id="apellidos" name="apellidos" required>
                    <i class="mdi mdi-account"></i>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="email" class="form-control" id="email" name="email" required>
                    <i class="mdi mdi-account"></i>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Contraseña</label>
                    <input type="password" class="form-control" id="pass" name="pass">
                    <i class="mdi mdi-eye"></i>
                  </div>
                  <div class="mt-5">
                    <button type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium">Registrarse</button>
                  </div>
                  <div class="mt-5">
                  <a class="btn btn-block btn-info btn-lg font-weight-medium" href="index.php">Ingresar</a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- row ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="..\template\vendors\js\vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="..\template\js\off-canvas.js"></script>
  <script src="..\template\js\hoverable-collapse.js"></script>
  <script src="..\template\js\misc.js"></script>
  <script src="..\template\js\settings.js"></script>
  <script src="..\template\js\todolist.js"></script>
  <!-- endinject -->
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="../lib/sweetalert.min.js"></script>
  <script src="js/registrarse.js?sin_cache=<?php echo md5(time()); ?>"></script>
</body>

</html>