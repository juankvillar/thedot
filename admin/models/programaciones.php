<?php


Class Programaciones{

	public $con = NULL;
	public $usuario_id = NULL;

	public function __construct() {
		if( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ){
			$ajax = '../';
		 }else{
			$ajax = '';
		 }
		include_once($ajax.'../config/init_db.php');
		$this->con = $mbd;
		$this->usuario_id = $usuario_id;
	}

	public function getProgramaciones(){
		$query = "SELECT p.*, e.estado, c.nombre, asi.asignados, ( p.cupos - asi.asignados ) disponibles
						FROM programaciones p
							inner join estados e
								on p.id_estado = e.id_estado
							inner join capacitaciones c
								on p.id_capacitacion = c.id_capacitacion
							left join ( select count( * ) asignados, id_programacion from asignaciones group by id_programacion  ) asi
								on asi.id_programacion = p.id_programacion;";
		$res = $this->con->query($query);
        $respuesta = $res->fetchAll();
		return $respuesta;
	}

	public function getCapacitaciones(){
		$query = "SELECT c.id_capacitacion, c.nombre FROM capacitaciones c
							inner join estados e
								on c.id_estado = e.id_estado  and c.id_estado = 1 order by nombre;";
		$res = $this->con->query($query);
        $respuesta = $res->fetchAll();
		return $respuesta;
	}

	public function getEstados(){
		$query = "SELECT * FROM estados;";
		$res = $this->con->query($query);
        $respuesta = $res->fetchAll();
		return $respuesta;
	}

	public function getProgramacion( $id_capacitacion ){
		$query = "SELECT * FROM capacitaciones where id_capacitacion = $id_capacitacion;";
		$res = $this->con->query($query);
        $respuesta = $res->fetch();
		//var_dump( $respuesta  );
		return $respuesta;
	}

	public function updateProgramacion( $p ){
		extract($p);
		 $query = "UPDATE capacitaciones
						SET
						nombre = '$nombre',
						estado = '$estado'
						WHERE id_capacitacion = $id_capacitacion;
						";
		$resultSet_usr = $this->con->exec($query);
            if ($resultSet_usr) {
                $respuesta['error'] = false;
                $respuesta['msj'] = 'Proceso exitoso';
                $respuesta['type'] = "success";
            } else {
                $respuesta['error'] = true;
                $respuesta['msj'] = 'No se realizó ningún cambio';
                $respuesta['type'] = "error";
            }
			return $respuesta;
	}

	public function InsertProgramacion( $p ){
		extract($p);
		$query = "INSERT INTO programaciones
					(
					id_capacitacion,
					cupos,
					desde,
					id_estado)
					VALUES
					(
					'$id_capacitacion',
					'$cupos',
					'$fecha $hora',
					'$id_estado');
					";
		$resultSet_usr = $this->con->exec($query);
            if ($resultSet_usr) {
                $respuesta['error'] = false;
                $respuesta['msj'] = 'proceso exitoso';
                $respuesta['type'] = "success";
            } else {
                $respuesta['error'] = true;
                $respuesta['msj'] = 'No se realizó ningún cambio';
                $respuesta['type'] = "error";
            }
			return $respuesta;
	}
	

}

