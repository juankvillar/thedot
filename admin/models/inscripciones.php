<?php


Class Inscripciones{

	public $con = NULL;
	public $usuario_id = NULL;

	public function __construct() {
		if( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ){
			$ajax = '../';
		 }else{
			$ajax = '';
		 }
		include_once($ajax.'../config/init_db.php');
		$this->con = $mbd;
		$this->usuario_id = $usuario_id;
	}

	public function getInscripciones(){
		$query = "SELECT a.id_asignacion, p.id_programacion, p.cupos, p.desde, p.id_estado, c.nombre, asi.asignados, ( p.cupos - asi.asignados ) disponibles from programaciones p
					inner join asignaciones a
						on p.id_programacion = a.id_programacion and id_usuario = {$this->usuario_id}
					inner join capacitaciones c
						on p.id_capacitacion = c.id_capacitacion
					left join ( select count( * ) asignados, id_programacion from asignaciones group by id_programacion  ) asi
									on asi.id_programacion = p.id_programacion;";
		$res = $this->con->query($query);
        $respuesta = $res->fetchAll();
		return $respuesta;
	}

	public function getInscripcionesPen(){

		$resInsc = $this->getInscripciones();

		$notIn = '0 ';
		foreach ($resInsc as $key => $value) {
			$notIn .= ', '.$value->id_programacion;
		}

		$query = "SELECT p.id_programacion, p.cupos, p.desde, p.id_estado, c.nombre, asi.asignados, ( p.cupos - asi.asignados ) disponibles from programaciones p
					inner join capacitaciones c
						on p.id_capacitacion = c.id_capacitacion
					left join ( select count( * ) asignados, id_programacion from asignaciones group by id_programacion  ) asi
									on asi.id_programacion = p.id_programacion
					where p.id_programacion not in ( $notIn );";
		$res = $this->con->query($query);
        $respuesta = $res->fetchAll();
		return $respuesta;
	}

	public function eliminar( $id_asignacion ){
		
		 $query = "DELETE FROM asignaciones
					WHERE id_asignacion = $id_asignacion;
					";
		$resultSet_usr = $this->con->exec($query);
            if ($resultSet_usr) {
                $respuesta['error'] = false;
                $respuesta['msj'] = 'Proceso exitoso';
                $respuesta['type'] = "success";
            } else {
                $respuesta['error'] = true;
                $respuesta['msj'] = 'No se realizó ningún cambio';
                $respuesta['type'] = "error";
            }
			return $respuesta;
	}

	public function inscribirme( $p ){
		extract($p);
		$query_aux = "SELECT count(*) cantidad FROM asignaciones where id_programacion = $id_programacion;";
		$res = $this->con->query($query_aux);
		$resaux = $res->fetch();
		
		$query_aux2 = "SELECT cupos FROM programaciones where id_programacion = $id_programacion;";
		$res2 = $this->con->query($query_aux2);
		$resaux2 = $res2->fetch();

		if ( $resaux->cantidad >= $resaux2->cupos ) {
				$respuesta['error'] = true;
                $respuesta['msj'] = 'La capacitación alcanzó el nivel máximo de inscritos';
                $respuesta['type'] = "error";
				return $respuesta;
		}

		$query = "INSERT INTO asignaciones
						(
						id_programacion,
						id_usuario,
						id_estado)
						VALUES
						(
						$id_programacion,
						{$this->usuario_id},
						1
						);
						";
		$resultSet_usr = $this->con->exec($query);
            if ($resultSet_usr) {
                $respuesta['error'] = false;
                $respuesta['msj'] = 'proceso exitoso';
                $respuesta['type'] = "success";
            } else {
                $respuesta['error'] = true;
                $respuesta['msj'] = 'No se realizó ningún cambio';
                $respuesta['type'] = "error";
            }
			return $respuesta;
	}
	

}

