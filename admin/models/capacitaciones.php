<?php


Class Capacitaciones{

	public $con = NULL;
	public $usuario_id = NULL;

	public function __construct() {
		if( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ){
			$ajax = '../';
		 }else{
			$ajax = '';
		 }
		include_once($ajax.'../config/init_db.php');
		$this->con = $mbd;
		$this->usuario_id = $usuario_id;
	}

	public function getCapacitaciones(){
		$query = "SELECT * FROM capacitaciones c
							inner join estados e
								on c.id_estado = e.id_estado;";
		$res = $this->con->query($query);
        $respuesta = $res->fetchAll();
		return $respuesta;
	}

	public function getEstados(){
		$query = "SELECT * FROM estados;";
		$res = $this->con->query($query);
        $respuesta = $res->fetchAll();
		return $respuesta;
	}

	public function getCapacitacion( $id_capacitacion ){
		$query = "SELECT * FROM capacitaciones where id_capacitacion = $id_capacitacion;";
		$res = $this->con->query($query);
        $respuesta = $res->fetch();
		//var_dump( $respuesta  );
		return $respuesta;
	}

	public function updateCapacitacion( $p ){
		extract($p);
		 $query = "UPDATE capacitaciones
						SET
						nombre = '$nombre',
						id_estado = '$estado'
						WHERE id_capacitacion = $id_capacitacion;
						";
		$resultSet_usr = $this->con->exec($query);
            if ($resultSet_usr) {
                $respuesta['error'] = false;
                $respuesta['msj'] = 'Proceso exitoso';
                $respuesta['type'] = "success";
            } else {
                $respuesta['error'] = true;
                $respuesta['msj'] = 'No se realizó ningún cambio';
                $respuesta['type'] = "error";
            }
			return $respuesta;
	}

	public function InsertCapacitacion( $p ){
		extract($p);
		$query = "INSERT INTO capacitaciones
						(
						nombre,
						id_estado
						)
						VALUES
						(
						'$nombre',
						'$estado'
						);
						";
		$resultSet_usr = $this->con->exec($query);
            if ($resultSet_usr) {
                $respuesta['error'] = false;
                $respuesta['msj'] = 'proceso exitoso';
                $respuesta['type'] = "success";
            } else {
                $respuesta['error'] = true;
                $respuesta['msj'] = 'No se realizó ningún cambio';
                $respuesta['type'] = "error";
            }
			return $respuesta;
	}
	

}

