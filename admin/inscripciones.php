<?php $loc = "cap"; ?>
<?php include('controllers/seguridad.php'); ?>
<?php include('controllers/inscripciones.php'); ?>
<?php include('../template/header.php'); ?>

<div class="row">
  <?php foreach ($Inscripciones as $key => $value) { ?>
  <div class="col-md-6 grid-margin">
    <div class="card">
      <div class="card-body">
        <h6 class="card-title mb-0"><?php echo $value->nombre ?></h6>
        <div class="d-flex justify-content-between align-items-center">
          <div class="d-inline-block pt-3">
            <div class="d-lg-flex">
              <h2 class="mb-0">Cupos <?php echo $value->disponibles ?> </h2>
              <div class="d-flex align-items-center ml-lg-2">
                <i class="mdi mdi-clock text-muted"></i>
                <small class="ml-1 mb-0">Inicia: <?php echo $value->desde ?></small>
              </div>
            </div>
            <small class="text-gray">Matriculados <?php echo $value->asignados ?>  <button data-id="<?php echo $value->id_asignacion ?>" type="button" class="eliminar btn btn-danger btn-xs"><i class="mdi mdi-delete"></i></button></small>
          </div>
          <div class="d-inline-block">
            <button type="button" class="btn btn-success btn-rounded btn-fw"><i class="mdi mdi-check text-white icon-sm">Inscrito</i></button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php }?>
  <?php foreach ($pendientes as $key => $value) { ?>
  <div class="col-md-6 grid-margin">
    <div class="card">
      <div class="card-body">
        <h6 class="card-title mb-0"><?php echo $value->nombre ?></h6>
        <div class="d-flex justify-content-between align-items-center">
          <div class="d-inline-block pt-3">
            <div class="d-lg-flex">
              <h2 class="mb-0">Cupos <?php echo is_null($value->disponibles) ? $value->cupos :  $value->disponibles; ?> </h2>
              <div class="d-flex align-items-center ml-lg-2">
                <i class="mdi mdi-clock text-muted"></i>
                <small class="ml-1 mb-0">Inicia: <?php echo $value->desde ?></small>
              </div>
            </div>
            <small class="text-gray">Matriculados <?php  echo is_null($value->asignados) ? 0 : $value->asignados; ?></small>
          </div>
          <div class="d-inline-block">
            <button data-id="<?php echo $value->id_programacion ?>" type="button" class="inscribirme btn btn-info btn-rounded btn-fw"><i class="mdi mdi-layers text-white icon-sm">Inscribirme</i></button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php }?>
  </div>
</div>
<?php include('../template/footer.php'); ?>
<script src="js/inscripciones.js?sin_cache=<?php echo md5(time()); ?>"></script>