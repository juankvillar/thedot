<?php $loc = "cap"; ?>
<?php include('controllers/seguridad.php'); ?>
<?php include('controllers/programaciones.php'); ?>
<?php include('../template/header.php'); ?>

<div class="row">
  <div class="col-12 grid-margin">
    <div class="card">
      <div class="card-body">
        <div class="template-demo d-flex justify-content-between flex-wrap">
          <button type="button" id="crear" class="btn btn-inverse-success btn-fw" data-toggle="modal" data-target="#modal"><i class=" mdi mdi-plus"></i>Crear programación</button>
          <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-4" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" ">Crear programación</h5>
                  <button  type=" button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                  <form id="frm">
                    <div class="form-group">
                      <label for="message-text" class="col-form-label">Capacitaciones:</label>
                      <select class="select2" style="width:100%" name="id_capacitacion" id="id_capacitacion" required>
                        <option value="">Seleccionar</option>
                        <?php foreach ($capacitaciones as $key => $val) { ?>
                          <option value="<?php echo $val->id_capacitacion ?>"><?php echo $val->nombre ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Cupos:</label>
                      <input type="number" min="1" class="form-control" name="cupos" id="cupos" required>
                    </div>
                    <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Fecha inicio:</label>
                      <input name="fecha" id="fecha" class="form-control" type="date" required onchange="validarF(event)">
                    </div>
                    <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Hora inicio:</label>
                      <input name="hora" id="hora" class="form-control" type="time"  required >
                    </div>
                    <div class="form-group">
                      <label for="message-text" class="col-form-label">Estado:</label>
                      <select class="select2" style="width:100%" name="id_estado" id="id_estado" required>
                        <option value="">Seleccionar</option>
                        <?php foreach ($estados as $key => $val) { ?>
                          <option value="<?php echo $val->id_estado ?>"><?php echo $val->estado ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-success">Guardar</button>
                    </div>

                  </form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-light" data-dismiss="modal">Cerrar</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <br>

        <div class="table-responsive">
          <table id="order-listing" class="table table-hover">
            <thead>
              <tr>
                <th>Order #</th>
                <th>Capacitación</th>
                <th>Fecha Ini.</th>
                <th>Cupos total</th>
                <th>Inscritos</th>
                <th>Cupos disponibles</th>
                <th>Estado</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($programaciones as $key => $value) { ?>
                <tr>
                  <td><? echo $key + 1 ?></td>
                  <td><? echo $value->nombre ?></td>
                  <td><? echo $value->desde ?></td>
                  <td><? echo $value->cupos ?></td>
                  <td><? echo  is_null( $value->asignados ) ? 0 : $value->asignados ; ?></td>
                  <td><? echo  is_null( $value->asignados ) ? 0 : $value->disponibles ; ?></td>
                  <?php $estado =  $value->id_estado == 1 ? 'success' : 'danger'; ?>
                  <td>
                    <label class="badge badge-<?php echo $estado; ?>"><?php echo $value->estado; ?></label>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include('../template/footer.php'); ?>
<script src="js/programaciones.js?sin_cache=<?php echo md5(time()); ?>"></script>