$("body").on('click', '.inscribirme', function (e) {
      e.preventDefault();
      id_programacion = $( this ).data('id');
      if ( !confirm("¿Está seguro de inscribirse a este curso?")) {
        return false;
      }
      $.ajax({
        type: 'POST',
        url: 'controllers/inscripciones.php',
        dataType: 'json',
        data: {opcn: "inscribirme", id_programacion:id_programacion},
    }).done(function (data) {
      swal(data.msj,'',data.type);
    })
    .fail(function (data) {
        //swal('No se realizó ningún cambio','','error');
    })
  });

  $("body").on('click', '.eliminar', function (e) {
    e.preventDefault();
    id_asignacion = $( this ).data('id');
    if ( !confirm("¿Está seguro de eliimnar esta inscripción?")) {
      return false;
    }
    $.ajax({
      type: 'POST',
      url: 'controllers/inscripciones.php',
      dataType: 'json',
      data: {opcn: "eliminar", id_asignacion:id_asignacion},
  }).done(function (data) {
    swal(data.msj,'',data.type);
  })
  .fail(function (data) {
      //swal('No se realizó ningún cambio','','error');
  })
});

  $("body").on('click', '.swal-button', function (e) {
    location.reload()
  });

  $("body").on('click', '.swal-overlay', function (e) {
    location.reload()
  });
