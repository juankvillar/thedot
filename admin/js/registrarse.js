$('#frm').submit(function (e) { 
    e.preventDefault();
    let datos = $( this ).serializeArray();
        datos.push({name: 'opcn' , value: 'registrarse' })

    $.ajax({
        type: 'post',
        url: 'controllers/registrarse.php',
        dataType: 'json',
        data: datos,
    }).done(function (data) {
        
        swal(data.msj,'',data.type);
    })
    .fail(function (data) {
        swal('Error en la solicitud ','','error');
    })
});

$("body").on('click', '.swal-button', function (e) {
    window.location.href = "index.php"
  });

  $("body").on('click', '.swal-overlay', function (e) {
    window.location.href = "index.php"
  });