let opcn = ""
let id_capacitacion = '';

$('#frm').submit(function (e) { 
    e.preventDefault();
    let datos = $( this ).serializeArray();
        datos.push({name: 'opcn' , value: opcn })
        datos.push({name: 'id_capacitacion' , value: id_capacitacion })

    $.ajax({
        type: 'post',
        url: 'controllers/capacitaciones.php',
        dataType: 'json',
        data: datos,
    }).done(function (data) {
        swal(data.msj,'',data.type);
    })
    .fail(function (data) {
        swal('No se realizó ningún cambio','','error');
    })
});

$("body").on('click', '.editar', function (e) {
      e.preventDefault();
      id_capacitacion = $( this ).data('id');
      opcn = 'editar';
      $.ajax({
        type: 'POST',
        url: 'controllers/capacitaciones.php',
        dataType: 'json',
        data: {opcn: "getCapacitacion", id_capacitacion:id_capacitacion},
    }).done(function (data) {
        $("#nombre").val(data.nombre);
        $("#estado").val(data.id_estado).trigger('change')
    })
    .fail(function (data) {
        //swal('No se realizó ningún cambio','','error');
    })
  });

  (function($) {
    'use strict';
    $(function() {
      $('#mytable').DataTable({
        "aLengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
        "iDisplayLength": 10,
        "language": { search: "" }
      });
      $('#mytable').each(function(){
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.removeClass('form-control-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.removeClass('form-control-sm');
      });
    });
  })(jQuery);


  $("#crear").click(function (e) { 
    e.preventDefault();
    opcn =  "crear"
    document.getElementById("frm").reset();
  });

  $("body").on('click', '.swal-button', function (e) {
    location.reload()
  });

  $("body").on('click', '.swal-overlay', function (e) {
    location.reload()
  });
