let opcn = ""
let id_programacion = '';

$('#frm').submit(function (e) { 
    e.preventDefault();
    let hora = $('#hora').val();
    arrHora = hora.split(':')
    if( arrHora[0] < 10 || arrHora[0] > 22 || ( arrHora[0] >= 22 && arrHora[1] > 00  )){
      alert('hora invalida, escoger un rango entre 10:00 y 22:00');
      return false;
    }

    let datos = $( this ).serializeArray();
        datos.push({name: 'opcn' , value: opcn })
          datos.push({name: 'id_programacion' , value: id_programacion })
        

    $.ajax({
        type: 'post',
        url: 'controllers/programaciones.php',
        dataType: 'json',
        data: datos,
    }).done(function (data) {
        swal(data.msj,'',data.type);
    })
    .fail(function (data) {
        swal('No se realizó ningún cambio','','error');
    })
});

$("body").on('click', '.editar', function (e) {
      e.preventDefault();
      id_programacion = $( this ).data('id');
      opcn = 'editar';
      $.ajax({
        type: 'POST',
        url: 'controllers/programaciones.php',
        dataType: 'json',
        data: {opcn: "getCapacitacion", id_programacion:id_programacion},
    }).done(function (data) {
        $("#nombre").val(data.nombre);
        $("#estado").val(data.id_estado).trigger('change')
    })
    .fail(function (data) {
        //swal('No se realizó ningún cambio','','error');
    })
  });

  (function($) {
    'use strict';
    $(function() {
      $('#mytable').DataTable({
        "aLengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
        "iDisplayLength": 10,
        "language": { search: "" }
      });
      $('#mytable').each(function(){
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.removeClass('form-control-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.removeClass('form-control-sm');
      });
    });
  })(jQuery);


  $("#crear").click(function (e) { 
    e.preventDefault();
    opcn =  "crear"
    document.getElementById("frm").reset();
  });

  $("body").on('click', '.swal-button', function (e) {
    location.reload()
  });

  $("body").on('click', '.swal-overlay', function (e) {
    location.reload()
  });


  function validarF(e) {
    const dia = (new Date(e.target.value)).getDay();
    console.log(dia)
    if ( dia >= 5 ) { //Cualquier día menor que Viernes
      e.target.value = ""; //Resetear la fecha
      alert('Fecha invalida, escoger solo días de lunes a viernes')
      //swal('No se realizó ningún cambio','','error');
    }
  
  }