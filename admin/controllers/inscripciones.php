<?php

$method = $_SERVER['REQUEST_METHOD'];

if (isset($_POST['opcn'])) {
    include_once('../models/inscripciones.php');
    $obj = new Inscripciones();
    switch ($_POST['opcn']) {
        case 'inscribirme':
            $res_data = $obj->inscribirme($_POST);
            break;
        case 'eliminar':
            $res_data = $obj->eliminar($_POST['id_asignacion']);
            break;
    } // fin switch $_POST['opcn']
    echo json_encode($res_data);
} else {
    include_once('models/inscripciones.php');
    $obj = new Inscripciones();

    if (isset($_GET['opcn'])) {
        switch ($_GET['opcn']) {
            case 'editar':
                break;
            default:
                header('Location: index.php');
                break;
        }
    } else {
        $Inscripciones = $obj->getInscripciones();
        $pendientes = $obj->getInscripcionesPen();
        
    }
}
