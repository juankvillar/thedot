<?php
@session_start();
if ( $_SESSION['id_rol'] !=1 ) {
    header('Location: inscripciones.php');
}
//print_r($_POST);
$method = $_SERVER['REQUEST_METHOD'];

if (isset($_POST['opcn'])) {
    include_once('../models/programaciones.php');
    $obj = new Programaciones();
    switch ($_POST['opcn']) {
        case 'crear':
            $res_data = $obj->InsertProgramacion($_POST);
            break;
        case 'editar':
            $res_data = $obj->updateProgramacion($_POST);
            break;
        case 'getCapacitacion':
            $res_data = $obj->getProgramacion($_POST['id_capacitacion']);
            break;
    } // fin switch $_POST['opcn']
    echo json_encode($res_data);
} else {
    include_once('models/programaciones.php');
    $obj = new Programaciones();

    if (isset($_GET['opcn'])) {
        switch ($_GET['opcn']) {
            case 'editar':
                if (!isset($_GET['id'])) {
                    header('Location: index.php');
                }
                $asesor = $obj->getProgramacion($_GET['id']);
                break;
            default:
                header('Location: index.php');
                break;
        }
    } else {
        $programaciones = $obj->getProgramaciones();
        $capacitaciones = $obj->getCapacitaciones();
        $estados = $obj->getEstados();
    }
}
