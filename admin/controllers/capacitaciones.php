<?php
@session_start();
if ( $_SESSION['id_rol'] !=1 ) {
    header('Location: inscripciones.php');
}

$method = $_SERVER['REQUEST_METHOD'];

if (isset($_POST['opcn'])) {
    include_once('../models/capacitaciones.php');
    $obj = new Capacitaciones();
    switch ($_POST['opcn']) {
        case 'crear':
            $res_data = $obj->insertCapacitacion($_POST);
            break;
        case 'editar':
            $res_data = $obj->updateCapacitacion($_POST);
            break;
        case 'getCapacitacion':
            $res_data = $obj->getCapacitacion($_POST['id_capacitacion']);
            break;
    } // fin switch $_POST['opcn']
    echo json_encode($res_data);
} else {
    include_once('models/capacitaciones.php');
    $obj = new Capacitaciones();

    if (isset($_GET['opcn'])) {
        switch ($_GET['opcn']) {
            case 'editar':
                if (!isset($_GET['id'])) {
                    header('Location: index.php');
                }
                $asesor = $obj->getCapacitacion($_GET['id']);
                break;
            default:
                header('Location: index.php');
                break;
        }
    } else {
        $capacitaciones = $obj->getCapacitaciones();
        $estados = $obj->getEstados();
        
    }
}
