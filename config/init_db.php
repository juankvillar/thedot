<?php

// mostarr errores PHP
$desarrollo = true;
if ( $desarrollo ) {
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
}

// define zona horaria
date_default_timezone_set('America/Bogota');
set_time_limit(0);

// varia global con la fecha actual
@session_start();
$usuario_id =  isset($_SESSION['id_usuario']) ? $_SESSION['id_usuario'] : '' ;
define('FECHA', date('Y-m-d H:i:s'));
define('USUARIO_ID', $usuario_id);

//conexion a la base de datos
$BDusuario    = 'root';
$BDcontrasena = 'root';
$BDbase_datos =  'thedot';

// configuracion PDO
$options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ, PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING);
$mbd = new PDO("mysql:host=mysql;dbname=$BDbase_datos;charset=utf8", $BDusuario, $BDcontrasena, $options);